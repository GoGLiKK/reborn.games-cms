<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Mmocms;
use App\Models\Mmocms\IpGuard;
use Config;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Lang;
use Response;
use Session;

class AuthController extends Controller
{

    use AuthenticatesAndRegistersUsers;

    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/account';
    }

    public function getLogin()
    {
        return Mmocms::printView('auth.login', Lang::get('meta.AuthorizationTitle'));
    }

    public function getRegister()
    {
        return Mmocms::printView('auth.register', Lang::get('meta.RegistrationTitle'));
    }

    public function postLogin(Request $request)
    {

        IpGuard::defineCaptcha('auth');

            $rules = ['email' => 'required|email', 'password' => 'required', 'g-recaptcha-response' => 'required',];

        $this->validate($request, $rules);

        $credentials = $request->only('email', 'password');

        if (($this->auth->attempt($credentials, $request->has('remember'))) and ($request->ajax())) {
            return Response::json(['result' => 'success', 'redirect' => $this->redirectPath()]);
        } elseif ($this->auth->attempt($credentials, $request->has('remember'))) {
            return redirect()->intended($this->redirectPath());
        }

        IpGuard::addAttempt('auth');

        if ($request->ajax()) {
            return Response::json(['error' => $this->getFailedLoginMessage()], 422);
        } else {
            return redirect($this->loginPath())
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => $this->getFailedLoginMessage(),
                ]);
        }
    }

    protected function getFailedLoginMessage()
    {
        return 'Неверный логин или пароль';
    }
}
