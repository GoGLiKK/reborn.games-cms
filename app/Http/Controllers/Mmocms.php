<?php namespace App\Http\Controllers;

use App;
use App\Models\Mmocms\StaticPages;
use Auth;
use Cache;
use Config;
use DB;
use Illuminate\Http\RedirectResponse;
use Lang;
use Redirect;
use Route;
use Session;
use Validator;
use View;

class Mmocms extends Controller
{

    public function getIndex()
    {
        return Mmocms::printView('index', Lang::get('meta.Title'));
    }

    public function staticPage($name)
    {
        $page_content = StaticPages::on()
            ->where('language', '=', Session::get('language', Config::get('app.locale')))
            ->where('name', '=', $name)
            ->where('published', '=', '1')
            ->first();

        if ($page_content == null) {
            abort('404');
        } else {
            $page_content = $page_content->toArray();
            if (($page_content['is_index'] == 1) and (Route::getCurrentRoute()->getPath() == 'page/{name}')) {
                return new RedirectResponse(url('/'));
            } else {
                View::share('server_status', Mmocms::serverStatus('l2nwo.ru', 7777));
                View::share('server_online', Mmocms::serverOnline());
                if (Auth::guest()) {
                    View::share('user_name', null);
                } else {
                    View::share('user_name', Auth::user()->name);
                }
                View::share('forum_posts', Mmocms::lastForumPosts());
                return view(Config::get('mmocms.template').'.static_pages', $page_content);
            }
        }
    }

    public function setLanguage($language)
    {
        $rules = [
            'language' => 'in:en,ru',
        ];

        $validator = Validator::make(array('language' => $language), $rules);

        if ($validator->passes()) {
            Session::put('language', $language);
            return Redirect::back()->withInput();
        } else {
            return Redirect::intended('/');
        }
    }

    public static function printView($template, $title = 'default', $description = 'default', $keywords = 'default', $merge_data = array())
    {
        if ($title == 'default') {
            $title = Lang::get('meta.title');
        } elseif ($description == 'default') {
            $description = Lang::get('meta.description');
        } elseif ($keywords == 'default') {
            $keywords = Lang::get('meta.keywords');
        }
        $template_dir = Config::get('mmocms.template').'.'.$template;
        View::share('server_status', Mmocms::serverStatus('l2nwo.ru', 7777));
        View::share('server_online', Mmocms::serverOnline());
        View::share('forum_posts', Mmocms::lastForumPosts());
        if (Auth::guest()) {
            View::share('user_name', null);
        } else {
            View::share('user_name', Auth::user()->name);
        }
        return view($template_dir, array('language' => Session::get('language', Config::get('app.locale')), 'title' => $title, 'keywords' => $keywords, 'description' => $description), $merge_data);
    }

    public static function lastForumPosts()
    {
        if (!Cache::has('forum_posts')) {
            $posts = DB::connection('forum')->select("
            SELECT thread_id,title,node_id,last_post_date,user_id,last_post_username
            FROM xf_thread
            ORDER BY last_post_date DESC
            LIMIT 5");
            Cache::put('forum_posts', $posts, '1');
        }
        return Cache::get('forum_posts');
    }

    public static function serverOnline()
    {
        if (!Cache::has('server_online')) {
            $online = DB::connection('game_1')->select("SELECT Count(1) as online FROM characters WHERE characters.`online` = 1");
            Cache::put('server_online', $online, '1');
        }
        return Cache::get('server_online');
    }

    public static function serverStatus($ip, $port)
    {
        if (!Cache::has('server_status')) {
            $errno = 0;
            $errstr = '';
            $fp = @fsockopen($ip, $port, $errno, $errstr, 1);
            (!$fp) ? $status = false : $status = true;
            @fclose($fp);
            Cache::put('server_status', $status, '1');
        }
        return Cache::get('server_status');
    }
}
