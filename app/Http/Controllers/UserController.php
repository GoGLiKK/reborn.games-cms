<?php namespace App\Http\Controllers;

use App\Models\Mmocms\ChangePass;
use App\Models\Mmocms\IpGuard;
use App\Models\lineage2\Accounts;
use App\Models\Mmocms\UserAccounts;
use Cache;
use Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Lang;
use Illuminate\Http\Request;
use Response;
use View;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function getIndex()
    {
        return Mmocms::printView('profile.index', Lang::get('meta.ProfileTitle'), 'default', 'default', array('data'=> Accounts::getAccounts()));
    }

    public function getPassword()
    {
        $request = ChangePass::getRequest();

        if (isset($request)) {
            if (time() >= strtotime("$request->created_at +24 hour")) {
                ChangePass::deleteRequest();
                return Mmocms::printView('profile.password', Lang::get('meta.ProfileTitle'))->with('request', 'false');
            }
        }

        if ($request) {
            return Mmocms::printView('profile.password', Lang::get('meta.ProfileTitle'))->with('request', 'true');
        } else {
            return Mmocms::printView('profile.password', Lang::get('meta.ProfileTitle'))->with('request', 'false');
        }
    }

    public function postPassword(Request $request)
    {

        IpGuard::defineCaptcha('change_pass');

        if (IpGuard::getAttempts('change_pass') >= Config::get('mmocms.change_pass_attempts')) {
            $rules = ['old_password' => 'required', 'password' => 'required', 'password_repeat' => 'required', 'g-recaptcha-response' => 'required',];
        } else {
            $rules = ['old_password' => 'required', 'password' => 'required', 'password_repeat' => 'required',];
        }

        $this->validate($request, $rules);

        if (Input::get('password') != Input::get('password_repeat')) {
            return Redirect::back()
                ->withInput()->withErrors([
                    'password' => 'Введенные пароли не совпадают',
                ]);
        }

        if (Hash::check(Input::get('old_password'), Auth::user()->password)) {
            ChangePass::createRequest(Hash::make(Input::get('password')));
            $token_url = Config::get('app.url') . '/account/password/' . ChangePass::getRequest()->token;
            Mail::send('emails.change_password', array('token' => $token_url), function ($message) {
                $message->to(Auth::user()->email, 'MMOCMS')->subject('Security: Password change request');
            });
            return Response::json(['success' => 'Email conformation sent!']);
        }

        IpGuard::addAttempt('change_pass');

        if ($request->ajax()) {
            return Response::json(['error' => $this->getFailedMessage()], 422);
        } else {
            return Redirect::back()
                ->withErrors([
                    'email' => $this->getFailedMessage(),
                ]);
        }
    }

    public function resendPassword()
    {
        $request = ChangePass::getRequest();
        if (time() >= strtotime("$request->created_at +15 minutes")) {
            $token_url = Config::get('app.url') . '/account/password/' . ChangePass::getRequest()->token;
            Mail::send('emails.change_password', array('token' => $token_url), function ($message) {
                $message->to(Auth::user()->email, 'MMOCMS')->subject('Security: Password change request');
            });
            ChangePass::updateTime();
            Session::flash('success', 'Запрос успешно отправлен, проверьте входящие сообщения');
            return Redirect::back();
        } else {
            return Redirect::back()
                ->withErrors([
                    'time' => 'Не прошло 15 минут после последнего запроса, пожалуйста, подождите',
                ]);
        }
    }

    public function cancelPassword()
    {
        ChangePass::deleteRequest();
        Session::flash('success', 'Request succesfully canceled');
        return Redirect::back();
    }

    public function changePassword($token)
    {
        $request = ChangePass::checkRequest($token);
        ChangePass::makeRequest($request->email, $request->password);
        Session::flash('success', 'Password succesfully changed');
        return Redirect::back();
    }

    private function getFailedMessage()
    {
        return 'Введены неверные данные.';
    }

    public function getCreateAccount()
    {
        return Mmocms::printView('games.lineage2.addAccount', Lang::get('meta.ProfileTitle'));
    }

    public function postCreateAccount(Request $request)
    {

        $rules = ['login' => 'required', 'password' => 'required', 'password_repeat' => 'required',];

        $this->validate($request, $rules);

        if (Input::get('password') != Input::get('password_repeat')) {
            return Redirect::back()
                ->withInput()->withErrors([
                    'password' => 'Введенные пароли не совпадают',
                ]);
        }

        if (Accounts::checkAvailable(Input::get('login')) == false) {
            return Redirect::back()
                ->withInput()->withErrors([
                    'password' => 'Аккаунт уже существует',
                ]);
        }

        $password = base64_encode(pack("H*", hash('whirlpool', utf8_encode(Input::get('password')))));
        UserAccounts::addUserAccount(Input::get('login'));
        Accounts::addAccount(Input::get('login'), $password);

        Session::flash('success', 'Аккаунт успешно создан');
        return Redirect::to('/account/');
    }

    public function getDonate()
    {
        return Mmocms::printView('profile.donate', Lang::get('meta.ProfileTitle'));
    }
}
