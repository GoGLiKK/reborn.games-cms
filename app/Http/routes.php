<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::before(function () {
    App::setLocale(Session::get('language', Config::get('app.locale')));
});

Route::get('language/{lang}', 'Mmocms@setLanguage');

Route::get('page/{name}', 'Mmocms@staticPage');

Route::get('account/password/resend', 'UserController@resendPassword');
Route::get('account/password/cancel', 'UserController@cancelPassword');
Route::get('account/password/{token}', 'UserController@changePassword');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
    'account' => 'UserController',
    '/' => 'Mmocms',
]);
