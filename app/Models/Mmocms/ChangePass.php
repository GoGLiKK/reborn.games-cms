<?php namespace App\Models\Mmocms;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ChangePass extends Model
{

    public $timestamps = false;

    protected $table = 'password_change';

    protected $fillable = ['email', 'password', 'token', 'created_at'];

    public static function createRequest($password)
    {

        ChangePass::create(['email'=> Auth::user()->email, 'password'=> $password, 'token' => str_random(40), 'created_at' => date('Y-m-d H:i:s')])->save();
    }

    public static function getRequest()
    {

        return ChangePass::on()
            ->where('email', '=', Auth::user()->email)
            ->first();
    }

    public static function updateTime()
    {

        ChangePass::on()
            ->where('email', '=', Auth::user()->email)
            ->update(['created_at' => date('Y-m-d H:i:s')]);
    }

    public static function deleteRequest()
    {

        ChangePass::on()
            ->where('email', '=', Auth::user()->email)
            ->delete();
    }

    public static function checkRequest($token)
    {

        $request = ChangePass::on()
            ->where('email', '=', Auth::user()->email)
            ->where('token', '=', $token)
            ->first();

        ChangePass::deleteRequest();

        if (isset($request)) {
            if (time() >= strtotime("$request->created_at +24 hour")) {
                return 'You request passed duration';
            }
        } else {
            abort('404');
        }

        return $request;
    }

    public static function makeRequest($email, $password)
    {

        ChangePass::where(['email' => $email])->update(['password' => $password]);
    }
}
