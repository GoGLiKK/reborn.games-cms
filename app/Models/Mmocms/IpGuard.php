<?php namespace App\Models\Mmocms;

use Config;
use Illuminate\Database\Eloquent\Model;
use Request;

class IpGuard extends Model
{

    public $timestamps = false;

    protected $table = 'ip_guard';

    protected $fillable = ['ip', 'action', 'last_time', 'attempts'];

    public static function addAttempt($action)
    {

        $attempts = IpGuard::getAttempts($action);
        IpGuard::firstOrCreate(['ip' => Request::ip(), 'action' => $action])->save();
        IpGuard::where(['ip' => Request::ip(), 'action' => $action])->update(['last_time' => date('Y-m-d H:i:s'), 'attempts' => $attempts+1]);
    }

    public static function getAttempts($action)
    {

        $attempt = IpGuard::on()
            ->where('ip', '=', Request::ip())
            ->where('action', '=', $action)
            ->first();
        if (!isset($attempt->attempts)) {
            return 0;
        } else {
            return $attempt->attempts;
        }
    }

    public static function getLastAttempt($action)
    {

        return IpGuard::on()
            ->where('ip', '=', Request::ip())
            ->where('action', '=', $action)
            ->first();
    }

    public static function deleteAttempts($action)
    {

        return IpGuard::on()
            ->where('ip', '=', Request::ip())
            ->where('action', '=', $action)
            ->delete();
    }

    public static function defineCaptcha($action)
    {

        $last_auth = IpGuard::getLastAttempt($action);

        $time = Config::get('mmocms.time_between_actions');
        if ((!isset($last_auth)) or (time() >= strtotime("$last_auth->last_time +$time minutes"))) {
            IpGuard::deleteAttempts($action);
        }
    }
}
