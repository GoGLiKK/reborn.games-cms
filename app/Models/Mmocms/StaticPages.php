<?php namespace App\Models\Mmocms;

use Illuminate\Database\Eloquent\Model;

class StaticPages extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'static_pages';
}
