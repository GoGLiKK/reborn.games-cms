<?php namespace App\Models\Mmocms;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: John
 * Date: 10.04.2015
 * Time: 17:18
 */
class UserAccounts extends Model
{

    public $timestamps = false;
    protected $table = 'user_accounts';
    protected $fillable = ['email', 'game_account'];

    public static function getUserAccounts()
    {

        return UserAccounts::on()
            ->where('email', Auth::user()->email)
            ->lists('game_account');
    }

    public static function addUserAccount($account)
    {

        UserAccounts::create(['email' => Auth::user()->email, 'game_account' => $account]);
    }
}
