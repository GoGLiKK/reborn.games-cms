<?php namespace App\Models\lineage2;

use App\Models\Mmocms\UserAccounts;
use DB;
use Illuminate\Database\Eloquent\Model;
use Request;

/**
 * Created by PhpStorm.
 * User: John
 * Date: 10.04.2015
 * Time: 17:18
 */
class Accounts extends Model
{

    public $timestamps = false;
    protected $table = 'accounts';
    protected $fillable = ['login', 'password', 'access_level', 'last_ip', 'bonus'];

    public static function getAccounts()
    {
        $accounts_array = "'".implode("', '", UserAccounts::getUserAccounts())."'";
        return DB::connection('login_1')
            ->select("SELECT
            accounts.login,
            accounts.last_ip,
            accounts.access_level,
            accounts_lock.allow_hwid,
            Count(characters.char_name) as char_count
            FROM accounts
            LEFT JOIN characters ON accounts.login = characters.account_name
            LEFT JOIN accounts_lock ON accounts.login = accounts_lock.login
            WHERE
            accounts.login IN ($accounts_array)
            GROUP BY
            accounts.login,
            accounts.last_ip,
            accounts.access_level,
            accounts_lock.allow_hwid
            ");
    }

    public static function checkAvailable($account)
    {

        $row = Accounts::on('login_1')->where('login', $account)->first();

        if (isset($row)) {
            return false;
        }

        return true;
    }

    public static function addAccount($account, $password)
    {

        Accounts::on('login_1')->insert(array(['login' => $account, 'password' => $password, 'last_ip' => Request::ip()]));
    }
}
