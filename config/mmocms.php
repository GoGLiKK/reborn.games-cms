<?php

return [

    'time_between_actions' => '60',

    'auth_attempts' => '10',

    'change_pass_attempts' => '2',

    'index_page' => 'index',

    'template' => 'delusion',

    'login_servers' => [
        'login_1' => [
            'name'    => 'Login Server',
        ],
    ],

    'game_servers' => [
        'bartz' => [
            'name'    => 'Bartz Server',
            'connection'    => 'game_1',
            'login'    => 'login_1',
        ],
    ],

];
