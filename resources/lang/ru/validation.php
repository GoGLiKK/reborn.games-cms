<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| following language lines contain default error messages used by
	| validator class. Some of these rules have multiple versions such
	| as size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute должен быть принят.",
	"active_url"           => ":attribute не верный URL.",
	"after"                => ":attribute должны быть дата, после :date.",
	"alpha"                => ":attribute может содержать только латинские буквы.",
	"alpha_dash"           => ":attribute может содержать только буквы, цифры и дефис.",
	"alpha_num"            => ":attribute может содержать только латинские буквы и цифры.",
	"array"                => ":attribute должен быть массивом.",
	"before"               => ":attribute должны быть дата до :date.",
	"between"              => [
		"numeric" => ":attribute должен быть между :min и :max.",
		"file"    => ":attribute должен быть между :min и :max kilobytes.",
		"string"  => ":attribute должен быть между :min и :max characters.",
		"array"   => ":attribute должны быть между :min и :max items.",
	],
	"boolean"              => ":attribute поле должно быть истинным или ложным.",
	"confirmed"            => ":attribute подтверждение не совпадает.",
	"date"                 => ":attribute дата не действительно.",
	"date_format"          => ":attribute не соответствует формату :format.",
	"different"            => ":attribute и :other должны отличаться.",
	"digits"               => ":attribute должно быть :digits digits.",
	"digits_between"       => ":attribute должно быть между :min и :max числами.",
	"email"                => ":attribute должен быть действительный адрес электронной почты.",
	"filled"               => ":attribute поле обязательно для заполнения.",
	"exists"               => "Выбранный :attribute является недействительным.",
	"image"                => ":attribute должно быть изображением.",
	"in"                   => "Выбранный :attribute является недействительным.",
	"integer"              => ":attribute должен быть целым числом.",
	"ip"                   => ":attribute должен быть IP адрес.",
	"max"                  => [
		"numeric" => ":attribute may not be greater than :max.",
		"file"    => ":attribute may not be greater than :max kilobytes.",
		"string"  => ":attribute may not be greater than :max characters.",
		"array"   => ":attribute may not have more than :max items.",
	],
	"mimes"                => ":attribute должен быть a file of type: :values.",
	"min"                  => [
		"numeric" => ":attribute должен быть at least :min.",
		"file"    => ":attribute должен быть at least :min kilobytes.",
		"string"  => ":attribute должен быть at least :min characters.",
		"array"   => ":attribute must have at least :min items.",
	],
	"not_in"               => "selected :attribute is invalid.",
	"numeric"              => ":attribute должен быть a number.",
	"regex"                => ":attribute format is invalid.",
	"required"             => ":attribute поле требуется.",
	"required_if"          => ":attribute поле требуется когда :other is :value.",
	"required_with"        => ":attribute поле требуется когда :values присутствует.",
	"required_with_all"    => ":attribute поле требуется когда :values присутствует.",
	"required_without"     => ":attribute поле требуется когда :values не присутствует.",
	"required_without_all" => ":attribute поле требуется когда ни один из :values присутствуют.",
	"same"                 => ":attribute и :other другие должны совпадать.",
	"size"                 => [
		"numeric" => ":attribute должен быть :size.",
		"file"    => ":attribute должен быть :size килобайт.",
		"string"  => ":attribute должен быть :size символы.",
		"array"   => ":attribute должен содержать :size элементы.",
	],
	"unique"               => ":attribute уже принят.",
	"url"                  => ":attribute формат является недействительным.",
	"timezone"             => ":attribute должен быть часовой пояс.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

];
