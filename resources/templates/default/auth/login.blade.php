@extends('default.app')

@section('content')
    <script type="text/javascript">

        jQuery(function ($) {
            $("#ajax_authorization").submit(function (e) {
                e.preventDefault();
                registr();
            });
        })

        function registr() {
            var data = $("#ajax_authorization").serialize();

            $.ajax({
                type: 'POST',
                url: '{{ url('/auth/login') }}',
                data: data,
                dataType: 'json',
                success: callback,
                error: errorCallback
            });
        }

        function callback(response) {
            window.location.href = response.redirect_url;
        }

        function errorCallback(jqxhr) {
            var error = jQuery.parseJSON(jqxhr.responseText);
                if (error['g-recaptcha-response']) {
                $('#captchaShow').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            } else {
                $("#auth_err").fadeIn().removeClass('hidden').text(error['email']).text(error['password']).text(error['error']);
            }
        }

    </script>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Login</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger hidden" id="auth_err">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <div class="alert alert-danger hidden" id="auth_err" ></div>

					<form class="form-horizontal" id="ajax_authorization" role="form" method="POST" action="">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail Address</label>
                            <div class="col-md-6">
                                <input type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>
                            </div>
                        </div>
                        <div class="modal fade" id="captchaShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        {!! Recaptcha::render() !!}
                                        <button type="submit" class="btn btn-primary btn-block">Verify me</button>
                                    </div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Login</button>

								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
