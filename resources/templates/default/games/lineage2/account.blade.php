@extends('default.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Profile page</div>
				<div class="panel-body">
                    @if (\App\Http\Controllers\Lineage2\Lineage2AccountController::haveAccount())
                                 <div class="row">
                                 @foreach ($data as $account)
                                        <div class="col-xs-6 col-sm-3 nav-tabs-justified">
                                               <div class="bs-block">
                                                   <table class="table table-condensed table-noborder">
                                                     <tr>
                                                         <td>Account:</td>
                                                         <td class="text-info">{{ $account['account'] }}</td>
                                                     </tr>
                                                     <tr>
                                                        <td>Status</td>
                                                        <td class="text-success">Active</td>
                                                     </tr>
                                                    </table>
                                                     <table class="table table-condensed table-striped small-font">
                                                         <tr>
                                                           <td>Кол-во персонажей: </td>
                                                           <td>0</td>
                                                         </tr>
                                                         <tr>
                                                            <td>Дата крайнего захода:</td>
                                                            <td>нет</td>
                                                         </tr>
                                                          <tr>
                                                          <td>IP захода в игру:</td>
                                                          <td>нет</td>
                                                          </tr>
                                                           <tr>
                                                           <td>Привязка по HWID:</td>
                                                          <td>Отключена</td>
                                                            </tr>
                                                     </table>
                                               </div>
                                        </div>
                                  @endforeach
                         @else
                    @endif

                    @if(! \App\Http\Controllers\Lineage2\Lineage2AccountController::isLimited())
                              <div class="col-xs-6 col-sm-3">
                                 <div class="bs-block">
                                  <a class="create_account" href="/account/create">Создать аккаунт</a>
                                 </div>
                              </div>
                          @else
                              <div class="col-xs-6 col-sm-3">
                                 <div class="bs-block">
                                    <div class="create_account">Максимум {{ \App\Models\lineage2\AccountsLineage2Impl::ACCOUNT_LIMITED  }}</div>
                                 </div>
                              </div>
                          @endif
                     </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

