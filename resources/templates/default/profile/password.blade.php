@extends('default.app')

@section('content')
<script type="text/javascript">

    jQuery(function ($) {
        $("#ajax_change_password").submit(function (e) {
            e.preventDefault();
            registr();
        });
    })

    function registr() {
        var data = $("#ajax_change_password").serialize();

        $.ajax({
            type: 'POST',
            url: '{{ url('/account/password') }}',
            data: data,
            dataType: 'json',
            success: callback,
            error: errorCallback
        });
    }

    function callback(response) {
        $("#change_pass_response").fadeIn().removeClass('hidden').removeClass('alert-danger').addClass('alert-success').text(response.success);
        $("#ajax_change_password").remove();
    }

    function errorCallback(jqxhr) {
        var error = jQuery.parseJSON(jqxhr.responseText);
        if (error['g-recaptcha-response']) {
            $('#captchaShow').modal({
                backdrop: 'static',
                keyboard: false
            })
        } else {
            $("#change_pass_response").fadeIn().removeClass('hidden').addClass('alert-danger').text(error['error']);
        }
    }

</script>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Change password</div>
                @if ($request === 'true')
                    <div class="panel-body">

                        @if ( Session::has('success') )
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                            </div>
                        @endif
                        You already sent a request for password recovery. If the message still has not come, reset the query or request to send again<br/><br/>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <a href="/account/password/resend" class="btn btn-primary" type="submit">Resend message</a> or <a href="/account/password/cancel">cancel request</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
				<div class="panel-body">

                    @if ( Session::has('success') )
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert hidden" id="change_pass_response" ></div>
                    <form class="form-chage-pass form-horizontal" id="ajax_change_password">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Old password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="old_password" placeholder="Old password" required="" autofocus="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">New password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" placeholder="New password" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Repeat new password</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_repeat" placeholder="Repeat password" required="">
                                </div>
                            </div>

                            <div class="modal fade" id="captchaShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            {!! Recaptcha::render() !!}
                                            <button type="submit" class="btn btn-primary btn-block">Verify me</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label"></label>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" type="submit">Change password</button>
                                </div>
                            </div>
                        </div>
                    </form>
				</div>
                @endif
			</div>
		</div>
	</div>
</div>
@endsection
