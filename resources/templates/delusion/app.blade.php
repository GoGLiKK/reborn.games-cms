<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="{{$language}}" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{$title}}</title>
    <meta name="description" content="{{$description}}">
    <meta name="keywords" content="{{$keywords}}">
    <link id="favicon" rel="shortcut icon" href="{{ asset('/public/img/favicon.ico') }}" type="image/x-icon">

    <link rel="stylesheet" href="/resources/templates/delusion/assets/css/style.css" type="text/css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="/resources/templates/delusion/assets/js/jCProgress-1.0.3.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

            options = {
                img1: '/resources/templates/delusion/assets/img/v1.png',
                img2: '/resources/templates/delusion/assets/img/v2.png',
                speed: 25,
                percent: 1,
                showPercent: false,
                limit:
                @if ($server_online[0]->online < 2)
                    2
                @else
                    {{$server_online[0]->online/500*100}}
                @endif
            };

            myplugin = $('#server_online').cprogress(options);
            var myplugin;

        });
    </script>

</head>

<body>

<div class="wrapper">

    <div class="header">
        <ul>
            <li class="left"><a href="{{ url('/') }}">Главная</a></li>
            <li class="left"><a href="#">Игра</a>
                <ul>
                    <li><a href="#">Скачать файлы</a></li>
                    <li><a href="http://forum.delusion.ws/threads/%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5-delusion-x30.15/">О сервере</a></li>
                    <li><a href="http://forum.delusion.ws/forums/%D0%90%D0%BA%D1%86%D0%B8%D0%B8-%D0%B8-%D0%BC%D0%B5%D1%80%D0%BE%D0%BF%D1%80%D0%B8%D1%8F%D1%82%D0%B8%D1%8F.3/">Акции сервера</a></li>
                    <li><a href="http://forum.delusion.ws/threads/%D0%9F%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D0%B0-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B0-%D1%87%D0%B8%D1%82%D0%B0%D1%82%D1%8C-%D0%BD%D0%B5-%D0%BE%D0%B1%D1%8F%D0%B7%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE.12/">Правила</a></li>
                </ul>
            </li>
            <li class="left"><a href="http://forum.delusion.ws/">Форум</a></li>
            @if (Auth::guest())
                <li class="right"><a href="{{ url('/auth/login') }}">Личный кабинет</a></li>
                <li class="right"><a href="{{ url('/auth/register') }}">Регистрация</a></li>
            @else
                <li class="right"><a href="{{ url('/auth/logout') }}">Выйти<span class="exit"></span></a></li>
                <li class="right"><a class="user_name" href="{{ url('/auth/login') }}"><span class="user_icon"></span>{{$user_name}}</a></li>
            @endif
        </ul>
        <div class="clear"></div>
        <div class="server_status">
            @if ($server_status == 1)
                <div class="status"><span>Status: <span class="on">online</span></span></div>
            @else
                <div class="status"><span>Status: <span class="off">offline</span></span></div>
            @endif
            <div id="server_online"></div>
            <div class="server_name"><span>High Five x30</span></div>
        </div>
        <div class="logo"><a href="{{ url('/') }}"></a></div>
        <div class="start_game"><a href="#"></a></div>
    </div><!-- .header-->

    <div class="middle">

        <div class="container">
            <div class="content">
                @yield('content')
            </div><!-- .content-->
        </div><!-- .container-->

        <div class="left-sidebar">
            <div class="sidebar_title">Последние темы форума</div>
            @foreach ($forum_posts as $post)
                <div class="last_post">
                    <div class="topic"><a href="http://forum.delusion.ws/threads/{{$post->thread_id}}" title="{{$post->title}}">
                            @if (mb_strlen($post->title) > 15 )
                            {{$post->title = mb_substr($post->title, 0, 15)}}...
                            @else
                            {{$post->title}}
                            @endif
                    </a></div>
                    <a href="http://forum.delusion.ws/members/{{$post->user_id}}" class="user_profile"><span class="user"></span>{{$post->last_post_username}}</a>
                    <span class="date"><span class="time"></span>{{$post->last_post_date}}</span>
                </div>
            @endforeach
        </div><!-- .left-sidebar -->

    </div><!-- .middle-->

    <div class="footer">
        <div class="copyrights">
            <p>© 2015 Игровой портал  delusion.ws</p>

            <p>ДАННЫЙ СЕРВЕР ЯВЛЯЕТСЯ ТЕСТОВЫМ ВАРИАНТОМ ИГРЫ
                LINEAGE II И ПРЕДНАЗНАЧЕН ТОЛЬКО ДЛЯ ОЗНАКОМЛЕНИЯ ИГРОКОВ.
                ВСЕ ПРАВА ПРИНАДЛЕЖАТ КОМПАНИИ NCSOFT.</p>
        </div>
        <div class="lineage_copy">
            <span class="nc_soft"></span>
            <span class="lineage"></span>
        </div>
        <a class="design_copy" href="http://asaya.ru/index.html" target="blank"></a>
    </div><!-- .footer -->

</div><!-- .wrapper -->

</body>
</html>
