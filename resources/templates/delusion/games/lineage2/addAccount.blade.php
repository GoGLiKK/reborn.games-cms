@extends('delusion.master')

@section('content')


<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Добавить новый аккаунт</div>
				<div class="panel-body">

                    @if (count($errors) > 0)
						<div class="alert alert-danger" id="auth_err">
							<strong>Произошла ошибка</strong>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
                    <div class="alert alert-danger hidden" id="auth_err" ></div>
                         {!! Form::open(array('url' => '/account/create-account', 'class' => 'form-horizontal', 'id' => 'ajax_add_accout')) !!}

                             <div class="form-group">
                                {!! Form::label('name','Логин:', ['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-6">
                                    {!! Form::text('login', null, ['class' => 'form-control', 'autofocus' => '' ]) !!}
                                </div>
                             </div>

                             <div class="form-group">
                                 {!! Form::label('password','Пароль:', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                 {!! Form::password('password',  ['class' => 'form-control']) !!}
                                  </div>
                             </div>

                             <div class="form-group">
                                 {!! Form::label('password_repeat','Повторите пароль:', ['class' => 'col-md-4 control-label']) !!}
                                 <div class="col-md-6">
                                    {!! Form::password('password_repeat',  ['class' => 'form-control']) !!}
                                 </div>
                             </div>
                                      <button class="btn btn-primary faggotiny" type="submit">Создать аккаунт</button>
                         {!! Form::close() !!}
			</div>
		</div>
	</div>
	</div>
</div>
@endsection
