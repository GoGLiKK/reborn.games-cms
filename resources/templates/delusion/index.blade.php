@extends('delusion.app')

@section('content')
    <div class="news">
        <div class="image">
            <img src="/resources/templates/delusion/assets/img/news.jpg">
            <a href="http://forum.delusion.ws/threads/%D0%9E%D1%82%D0%BA%D1%80%D1%8B%D1%82%D0%B8%D0%B5-delusion-x30-8-%D0%BC%D0%B0%D1%8F-%D0%B2-19-00-gmt-3.23/">Подробнее...</a>
        </div>
        <div class="title"><a href="#">High Five x30 открытие 8 мая в 19:00 мск!</a></div>
        <div class="date">24.04.2015 15:36</div>
        <div class="news_text">

            Друзья! Первый сервер нашего комплекса Delusion x30 откроется 8 мая в 19:00.
            Мы постарались собрать все самое лучшее и отбросить лишнее.
            Мы подарим вам по настоящему качественный сервер, без лагов, багов и задержек.
            Потому что наши специалисты лучшие в этом деле! <br>
            Наша сборка зарекомендовала себя на многих крупных проектах! <br>
            Наш бюджет на рекламную кампанию действительно очень внушительный,
            что позволит собрать ОГРОМНЫЙ онлайн!<br>
            Присоединяйтесь, вместе мы сделаем, пожалуй, лучший игровой проект!

        </div>
        <div class="clear"></div>
    </div>
@endsection
