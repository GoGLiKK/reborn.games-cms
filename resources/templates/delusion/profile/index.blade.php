@extends('delusion.master')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Игровые аккаунты</div>

                @if ( Session::has('success') )
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                @endif
                <a class="create_account_link button yellow_omfg" href="/account/create-account">+ Добавить аккаунт</a>
				<div class="panel-body">
                    <div class="row accounts">
                        @foreach ($data as $account)
                            <div class="col-xs-6 col-sm-3 nav-tabs-justified">
                                <div class="bs-block">
                                        <div class="account_name background_faggotiny">
                                            <span class="background_faggotiny text-info">{{ $account->login }}</div>
                                        </div>
                                        <div>
                                            @if ($account->access_level >= 0)
                                                <span class="text-success bordered">Активен</span>
                                            @elseif ($account->access_level < 0)
                                                <span class="text-warning bordered">Забанен</span>
                                            @endif
                                        </div>
                                        <div class="account_information">
                                            <table class="table table-condensed table-striped small-font">
                                                <tr>
                                                    <td>Кол-во персонажей: </td>
                                                    <td>{{ $account->char_count }}</td>
                                                </tr>
                                                <tr>
                                                    <td>IP захода в игру:</td>
                                                    <td>{{ $account->last_ip }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Привязка по HWID:</td>
                                                    @if ($account->allow_hwid == null)
                                                        <td>Отключена</td>
                                                    @else
                                                        <td>Включена</td>
                                                    @endif
                                                </tr>
                                            </table>
                                        </div>
                            </div>
                        @endforeach
                    </div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
