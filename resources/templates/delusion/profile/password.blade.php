@extends('delusion.master')

@section('content')
<script type="text/javascript">

    jQuery(function ($) {
        $("#ajax_change_password").submit(function (e) {
            e.preventDefault();
            registr();
        });
    })

    function registr() {
        var data = $("#ajax_change_password").serialize();

        $.ajax({
            type: 'POST',
            url: '{{ url('/account/password') }}',
            data: data,
            dataType: 'json',
            success: callback,
            error: errorCallback
        });
    }

    function callback(response) {
        $("#change_pass_response").fadeIn().removeClass('hidden').removeClass('alert-danger').addClass('alert-success').text(response.success);
        $("#ajax_change_password").remove();
    }

    function errorCallback(jqxhr) {
        var error = jQuery.parseJSON(jqxhr.responseText);
        if (error['g-recaptcha-response']) {
            $('#captchaShow').modal({
                backdrop: 'static',
                keyboard: false
            })
        } else {
            $("#change_pass_response").fadeIn().removeClass('hidden').addClass('alert-danger').text(error['error']);
        }
    }

</script>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Смена пароля мастер аккаунта</div>
                @if ($request === 'true')
                    <div class="panel-body">

                        @if ( Session::has('success') )
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        {{ $error }}
                                    @endforeach
                            </div>
                        @endif
                            Вы уже отправили запрос на восстановление пароля. Если сообщение все еще не пришло, отмените запрос или запросите повторную отправку<br/><br/>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-4">
                                    <a href="/account/password/resend" class="btn btn-primary" type="submit">Отправить сообщение</a> или <a href="/account/password/cancel">отменить запрос</a>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
				<div class="panel-body">

                    @if ( Session::has('success') )
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif

                    <div class="alert hidden" id="change_pass_response" ></div>
                    <form class="form-chage-pass form-horizontal" id="ajax_change_password">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="row">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Старый пароль</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="old_password" required="" autofocus="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Новый пароль</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password" required="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Повторите новый пароль</label>
                                <div class="col-md-6">
                                    <input type="password" class="form-control" name="password_repeat" required="">
                                </div>
                            </div>

                                    <button class="btn btn-primary faggotiny" type="submit">Сменить пароль</button>
                        </div>
                    </form>
				</div>
                @endif
			</div>
		</div>
	</div>
</div>
@endsection
